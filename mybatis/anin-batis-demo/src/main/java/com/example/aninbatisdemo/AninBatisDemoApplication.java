package com.example.aninbatisdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AninBatisDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AninBatisDemoApplication.class, args);
    }

}
