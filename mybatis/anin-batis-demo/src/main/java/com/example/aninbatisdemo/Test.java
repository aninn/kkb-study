package com.example.aninbatisdemo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/***
 * @program     ：mybatis
 * @author      ：chenglining
 * @date        ：Created in 2021/6/1 16:09
 * @description ：
 * @version     ：B1.3.7
 */
public class Test {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>(Arrays.asList(1,2));
        list.add(3);
        System.err.println(list);

        boolean[] bs = new boolean[2];

        float[] fs = new float[1];
        double[] ds = new double[1];

        short[] ss = new short[1];
        int[] is = new int[1];
        long[] ls = new long[1];

        char[] cs = new char[1];
        String[] sss = new String[1];
        byte[] bss = new byte[1];
        Runtime.getRuntime().exit(0);
    }
}
