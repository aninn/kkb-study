package com.example.aninbatisdemo.mapper;

import com.example.aninbatisdemo.pojo.User;

/***
 * @program     ：kkb-study
 * @author      ：chenglining
 * @date        ：Created in 2021/5/20 19:40
 * @description ：
 * @version     ：B1.3.7
 */
public interface UserMapper {

    User queryUserById(User user);
}
