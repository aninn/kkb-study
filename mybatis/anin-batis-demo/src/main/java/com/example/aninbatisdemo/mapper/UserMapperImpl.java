package com.example.aninbatisdemo.mapper;

import com.example.aninbatis.sqlsession.SqlSession;
import com.example.aninbatis.sqlsession.SqlSessionFactory;
import com.example.aninbatisdemo.pojo.User;

/***
 * @program     ：kkb-study
 * @author      ：chenglining
 * @date        ：Created in 2021/5/25 16:26
 * @description ：
 * @version     ：B1.3.7
 */
public class UserMapperImpl implements UserMapper{

    private SqlSessionFactory sqlSessionFactory;

    public UserMapperImpl(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @Override
    public User queryUserById(User user) {
        SqlSession sqlSession = sqlSessionFactory.openSqlSession();
        return sqlSession.selectOne("userMapper.queryUserById",user);
    }
}
