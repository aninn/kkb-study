package com.example.aninbatisdemo;


import org.springframework.http.HttpHeaders;

/***
 * @program     ：mybatis
 * @author      ：chenglining
 * @date        ：Created in 2021/5/27 15:36
 * @description ：
 * @version     ：B1.3.7
 */
public class Test {

    public static void main(String[] args) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("1","1");
        int count = 0;
        while (true){
            count++;
            try {
                for (int i = 0; i < 10; i++) {
                    if(i == 5){
                        System.out.println(headers.get("1"));
                        System.out.println(1/0);
                    }else {
                        System.out.println(headers.get("1"));
                    }
                }
            }catch (Exception e){
                headers.set("1","2");
            }
            if(count == 2){
                break;
            }
        }
    }
}
