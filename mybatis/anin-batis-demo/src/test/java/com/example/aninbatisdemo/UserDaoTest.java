package com.example.aninbatisdemo;

import java.io.InputStream;

import com.example.aninbatis.sqlsession.SqlSessionFactory;
import com.example.aninbatis.sqlsession.SqlSessionFactoryBuilder;
import com.example.aninbatis.utils.Resources;
import com.example.aninbatisdemo.mapper.UserMapper;
import com.example.aninbatisdemo.mapper.UserMapperImpl;
import com.example.aninbatisdemo.pojo.User;
import org.junit.jupiter.api.Test;

public class UserDaoTest {

	/**
	 * 测试使用手写mybatis框架去实现的
	 */
	@Test
	public void testQueryUserById() {
		 // 全局配置文件
		String resource = "SqlMapConfig.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		// SqlSessionFactory的创建可能有几种创建方式，但是我还是不想要知道SqlSessionFactory的构造细节
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

		UserMapper userMapper = new UserMapperImpl(sqlSessionFactory);
		User param = new User();
		param.setId(1);
		User user = userMapper.queryUserById(param);
		System.out.println(user);
	}

}
