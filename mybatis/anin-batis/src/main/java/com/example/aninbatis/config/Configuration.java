package com.example.aninbatis.config;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

/**
 * 封装了数据源信息以及封装了CRUD标签的信息
 */
public class Configuration {

	/**
	 * 数据源信息
	 */
	private DataSource dataSource;

	/**
	 * 封装了CRUD标签的信息
	 * key:namespace.id
	 * value:对应的标签信息
	 */
	private Map<String, MappedStatement> mappedStatements = new HashMap<>();

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setMappedStatement(String statementId, MappedStatement mappedStatement) {
		mappedStatements.put(statementId, mappedStatement);
	}

	public MappedStatement getMappedStatementById(String statementId) {
		return mappedStatements.get(statementId);
	}
}
