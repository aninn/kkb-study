package com.example.aninbatis.config;


import com.example.aninbatis.sqlsource.SqlSource;

/**
 * 封装select等CRUD标签的信息
 */
public class MappedStatement {
	/**
	 * CRUD标签的ID
	 */
	private String statementId;
	/**
	 * 参数类型class
	 */
	private Class<?> parameterTypeClass;
	/**
	 * 返回值类型class
	 */
	private Class<?> resultTypeClass;
	/**
	 * CRUD标签类型
	 */
	private String statementType;
	/**
	 * 封装sql
	 */
	private SqlSource sqlSource;

	public MappedStatement(String statementId, Class<?> parameterTypeClass, Class<?> resultTypeClass,
			String statementType, SqlSource sqlSource) {
		this.statementId = statementId;
		this.parameterTypeClass = parameterTypeClass;
		this.resultTypeClass = resultTypeClass;
		this.statementType = statementType;
		this.sqlSource = sqlSource;
	}

	public String getStatementId() {
		return statementId;
	}

	public void setStatementId(String statementId) {
		this.statementId = statementId;
	}

	public Class<?> getParameterTypeClass() {
		return parameterTypeClass;
	}

	public void setParameterTypeClass(Class<?> parameterTypeClass) {
		this.parameterTypeClass = parameterTypeClass;
	}

	public Class<?> getResultTypeClass() {
		return resultTypeClass;
	}

	public void setResultTypeClass(Class<?> resultTypeClass) {
		this.resultTypeClass = resultTypeClass;
	}

	public String getStatementType() {
		return statementType;
	}

	public void setStatementType(String statementType) {
		this.statementType = statementType;
	}

	public SqlSource getSqlSource() {
		return sqlSource;
	}

	public void setSqlSource(SqlSource sqlSource) {
		this.sqlSource = sqlSource;
	}

}
