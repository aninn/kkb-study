package com.example.aninbatis.executor;

import com.example.aninbatis.config.Configuration;
import com.example.aninbatis.config.MappedStatement;

import java.util.List;

/**
 * 执行器
 */
public interface Executor {

	/**
	 * 
	 * @param mappedStatement
	 *            获取sql语句和入参出参等信息
	 * @param configuration
	 *            获取数据源对象
	 * @param param
	 *            入参对象
	 * @return
	 */
	<T> List<T> query(MappedStatement mappedStatement, Configuration configuration, Object param);
}
