package com.example.aninbatis.sqlnode;

import com.example.aninbatis.sqlsource.DynamicContext;

import java.util.List;

/**
 * 以集合的方式存储TextSqlNode，StaticTextSqlNode，IfSqlNode，WhereSqlNode，ForeachSqlNode等
 */
public class MixedSqlNode implements SqlNode {

	private List<SqlNode> sqlNodes;

	public MixedSqlNode(List<SqlNode> sqlNodes) {
		this.sqlNodes = sqlNodes;
	}

	@Override
	public void apply(DynamicContext context) {
		for (SqlNode sqlNode : sqlNodes) {
			sqlNode.apply(context);
		}
	}

}
