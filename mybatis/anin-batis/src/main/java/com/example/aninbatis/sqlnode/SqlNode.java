package com.example.aninbatis.sqlnode;


import com.example.aninbatis.sqlsource.DynamicContext;

/**
 * 提供对sql脚本的解析
 */
public interface SqlNode {

	/**
	 * 应用解析
	 *
	 * @param context
	 */
	void apply(DynamicContext context);
}
