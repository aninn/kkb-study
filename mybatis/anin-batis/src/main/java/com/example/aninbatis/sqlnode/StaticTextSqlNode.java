package com.example.aninbatis.sqlnode;


import com.example.aninbatis.sqlsource.DynamicContext;

/**
 * 首先是文本内容，其实是内容不包含${}，可能包含#{}
 * 如果是这种SqlNode，则存储在RawSqlSource中
 */
public class StaticTextSqlNode implements SqlNode {

	private String sqlText;

	public StaticTextSqlNode(String sqlText) {
		super();
		this.sqlText = sqlText;
	}

	@Override
	public void apply(DynamicContext context) {
		// sql文本追加
		context.appendSql(sqlText);
	}

}
