package com.example.aninbatis.sqlsession;


import com.example.aninbatis.config.Configuration;

/**
 * 默认sqlsession工厂实现类
 */
public class DefaultSqlSessionFactory implements SqlSessionFactory {

	// 等待注入
	private Configuration configuration;
	
	public DefaultSqlSessionFactory(Configuration configuration) {
		super();
		this.configuration = configuration;
	}

	@Override
	public SqlSession openSqlSession() {
		return new DefaultSqlSession(configuration);
	}

}
