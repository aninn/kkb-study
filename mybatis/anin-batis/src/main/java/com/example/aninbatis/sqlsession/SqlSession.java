package com.example.aninbatis.sqlsession;

import java.util.List;

/**
 * 表示一个sql会话，就是一次CRUD操作
 */
public interface SqlSession {

	/**
	 * 查询单条数据
	 */
	<T> T selectOne(String statementId, Object param);

	/**
	 * 查询列表数据
	 */
	<T> List<T> selectList(String statementId, Object param);
}
