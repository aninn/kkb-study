package com.example.aninbatis.sqlsession;

/**
 * SqlSession工厂
 */
public interface SqlSessionFactory {

	/**
	 * 开启SqlSession
	 */
	SqlSession openSqlSession();
}
