package com.example.aninbatis.sqlsession;

import java.io.InputStream;
import java.io.Reader;

import com.example.aninbatis.config.Configuration;
import com.example.aninbatis.config.XMLConfigParser;
import com.example.aninbatis.utils.DocumentUtils;
import org.dom4j.Document;


/**
 * 使用构建者模式对SqlSessionFactory进行创建
 */
public class SqlSessionFactoryBuilder {

	/**
	 * 构建SqlSessionFactory
	 */
	public SqlSessionFactory build(InputStream inputStream) {
		// 获取Configuration对象
		Document document = DocumentUtils.readDocument(inputStream);
		XMLConfigParser configParser = new XMLConfigParser();
		Configuration configuration = configParser.parse(document.getRootElement());
		return build(configuration);
	}

	public SqlSessionFactory build(Reader reader) {
		return null;
	}

	private SqlSessionFactory build(Configuration configuration) {
		return new DefaultSqlSessionFactory(configuration);
	}
}
