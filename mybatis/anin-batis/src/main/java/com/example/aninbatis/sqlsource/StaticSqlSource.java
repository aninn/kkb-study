package com.example.aninbatis.sqlsource;

import java.util.List;

/**
 * 封装DynamicSqlSource和RawSqlSource处理之后的结果
 */
public class StaticSqlSource implements SqlSource {

	private String sql;

	private List<ParameterMapping> parameterMappings;

	public StaticSqlSource(String sql, List<ParameterMapping> parameterMappings) {
		super();
		this.sql = sql;
		this.parameterMappings = parameterMappings;
	}

	@Override
	public BoundSql getBoundSql(Object param) {
		return new BoundSql(sql, parameterMappings);
	}

}
