package com.example.aninbatis.utils;

import java.io.InputStream;
import java.io.Reader;

public class Resources {

	// 获取配置文件为流
	public static InputStream getResourceAsStream(String resource) {
		return Resources.class.getClassLoader().getResourceAsStream(resource);
	}

	public static Reader getResourceAsReader(String resource) {
		return null;
	}
}
