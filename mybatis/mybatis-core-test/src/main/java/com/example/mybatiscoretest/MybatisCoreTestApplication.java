package com.example.mybatiscoretest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisCoreTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisCoreTestApplication.class, args);
    }

}
