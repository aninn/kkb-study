package com.example.mybatiscoretest.mapper;


import com.example.mybatiscoretest.pojo.User;

/***
 * @program     ：kkb-study
 * @author      ：chenglining
 * @date        ：Created in 2021/5/20 19:40
 * @description ：
 * @version     ：B1.3.7
 */
public interface UserMapper {

    User query(User user);
}
