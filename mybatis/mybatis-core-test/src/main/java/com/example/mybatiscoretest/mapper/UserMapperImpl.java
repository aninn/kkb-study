package com.example.mybatiscoretest.mapper;

import com.example.mybatiscoretest.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;

/***
 * @program     ：mybatis
 * @author      ：chenglining
 * @date        ：Created in 2021/6/4 11:45
 * @description ：
 * @version     ：B1.3.7
 */
public class UserMapperImpl implements UserMapper{

    private SqlSessionFactory sqlSessionFactory;

    public UserMapperImpl(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @Override
    public User query(User user) {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        return sqlSession.selectOne("com.example.mybatiscoretest.mapper.UserMapper.query",user);
    }
}
