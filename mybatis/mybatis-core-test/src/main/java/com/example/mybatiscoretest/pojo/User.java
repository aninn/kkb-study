package com.example.mybatiscoretest.pojo;

import lombok.Data;

import java.io.Serializable;

/***
 * @program     ：kkb-study
 * @author      ：chenglining
 * @date        ：Created in 2021/5/20 19:39
 * @description ：
 * @version     ：B1.3.7
 */
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 0L;

    private Integer id;
    private String name;
}
