package com.example.mybatiscoretest;

import com.example.mybatiscoretest.mapper.UserMapper;
import com.example.mybatiscoretest.mapper.UserMapperImpl;
import com.example.mybatiscoretest.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.io.InputStream;

@SpringBootTest
class MybatisCoreTestApplicationTests {

    @Test
    void test1() throws IOException {
        // 全局配置文件
        String resource = "SqlMapConfig.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        UserMapper userMapper = new UserMapperImpl(sqlSessionFactory);
        User param = new User();
        param.setId(1);
        User user = userMapper.query(param);
        System.out.println(user);
    }

}
