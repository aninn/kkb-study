package com.example.mybatisdemo.mapper;

import com.example.mybatisdemo.pojo.Order;
import com.example.mybatisdemo.pojo.OrderExt;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/***
 * @program     ：kkb-study
 * @author      ：chenglining
 * @date        ：Created in 2021/5/20 19:40
 * @description ：
 * @version     ：B1.3.7
 */
@Mapper
public interface OrderMapper {

    OrderExt query(Integer id);

    List<Order> simpleQuery(Integer id);
}
