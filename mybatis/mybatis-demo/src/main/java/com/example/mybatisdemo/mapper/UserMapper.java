package com.example.mybatisdemo.mapper;

import com.example.mybatisdemo.pojo.User;
import com.example.mybatisdemo.pojo.UserExt;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/***
 * @program     ：kkb-study
 * @author      ：chenglining
 * @date        ：Created in 2021/5/20 19:40
 * @description ：
 * @version     ：B1.3.7
 */
@Mapper
public interface UserMapper {

    UserExt query(Integer id);

    UserExt query2(Integer id);

    User findUserById(Integer id);

    List<User> findUserList(User user);

    List<User> findUserListByIds(List<Integer> ids);

    List<User> selectUserByBind(User user);
}
