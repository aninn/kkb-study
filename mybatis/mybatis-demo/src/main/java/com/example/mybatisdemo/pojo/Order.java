package com.example.mybatisdemo.pojo;

import lombok.Data;

/***
 * @program     ：kkb-study
 * @author      ：chenglining
 * @date        ：Created in 2021/5/20 19:44
 * @description ：
 * @version     ：B1.3.7
 */
@Data
public class Order {

    private Integer id;
    private Integer userId;
    private String desc;
}
