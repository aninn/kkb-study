package com.example.mybatisdemo.pojo;

import lombok.Data;

import java.util.List;

/***
 * @program     ：kkb-study
 * @author      ：chenglining
 * @date        ：Created in 2021/5/21 8:21
 * @description ：
 * @version     ：B1.3.7
 */
@Data
public class UserExt extends User {

    List<Order> orders;

    @Override
    public String toString() {
        return "UserExt{" +
                "orders=" + orders +
                "} " + super.toString();
    }
}
