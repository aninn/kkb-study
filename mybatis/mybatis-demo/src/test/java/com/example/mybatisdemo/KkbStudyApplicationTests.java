package com.example.mybatisdemo;

import com.example.mybatisdemo.mapper.OrderMapper;
import com.example.mybatisdemo.mapper.UserMapper;
import com.example.mybatisdemo.pojo.OrderExt;
import com.example.mybatisdemo.pojo.User;
import com.example.mybatisdemo.pojo.UserExt;
import lombok.SneakyThrows;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class KkbStudyApplicationTests {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Test
    void test1() {
        OrderExt query = orderMapper.query(1);
        System.err.println(query);
    }

    @Test
    void test2() {
        UserExt query = userMapper.query(2);
        System.err.println(query);
    }

    @Test
    void test3() {
        UserExt query = userMapper.query2(2);
        System.err.println(query.getName());
        System.err.println(query);
    }

    @SneakyThrows
    @Test
    void test4() {
        // 全局配置文件
        String resource = "SqlMapConfig.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        // SqlSessionFactory的创建可能有几种创建方式，但是我还是不想要知道SqlSessionFactory的构造细节
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        // 第一次查询ID为1的用户，去缓存找，找不到就去查找数据库
        User user1 = mapper.findUserById(1);
        System.out.println(user1);
        // 第二次查询ID为1的用户
        User user2 = mapper.findUserById(1);
        System.out.println(user2); sqlSession.close();
    }

    @Test
    void test5() {
        // 第一次查询ID为1的用户，去缓存找，找不到就去查找数据库
        User user1 = userMapper.findUserById(1);
        System.out.println(user1);
        // 第二次查询ID为1的用户
        User user2 = userMapper.findUserById(1);
        System.out.println(user2);
    }

    @Test
    void test6() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        List<User> userListByIds = userMapper.findUserListByIds(list);
        System.err.println(userListByIds);
    }

    @Test
    void test7() {
        User user = new User();
        user.setName("三");
        List<User> userListByIds = userMapper.selectUserByBind(user);
        System.err.println(userListByIds);
    }

    @SneakyThrows
    @Test
    void test8() {

    }



}
